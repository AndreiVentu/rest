﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Models;
using DataLayer;

namespace BusinessLayer
{
    public class LibraryService : ILibraryService
    {
        private readonly ILibraryRepository libraryRepository;

        public LibraryService()
        {
            this.libraryRepository = new LibraryRepository();
        }

        public Guid AddLibrary(BusinessLayer.Models.LibraryWriteModel library)
        {
            var dataModelLibrary = new DataLayer.Models.Library
            {
                Id = Guid.NewGuid(),
                Name = library.Name,
                EmailAddress = library.EmailAddress,
                PhysicalAddress = library.PhysicalAddress,
                PhoneNumber = library.PhoneNumber
            };

            libraryRepository.AddLibrary(dataModelLibrary);

            return dataModelLibrary.Id;
        }

        public IEnumerable<LibraryReadModel> GetLibraries(bool ascendingOrder)
        {
            var dataModelLibraries = libraryRepository.GetLibraries().Select(x => Map(x));
            if (ascendingOrder)
            {
                return dataModelLibraries.OrderBy(x => x.Name);
            }

            return dataModelLibraries.OrderByDescending(x => x.Name);
        }

        public LibraryReadModel GetLibrary(Guid id)
        {
            var library = libraryRepository.GetLibrary(id);
            if (library == null) return null;

            return Map(libraryRepository.GetLibrary(id));
        }

        public bool UpdateLibrary(Guid id, LibraryWriteModel library)
        {
            var entityExists = libraryRepository.UpdateLibrary(Map(id, library));
            return entityExists;
        }

        void ILibraryService.DeleteLibrary(int id)
        {
            throw new NotImplementedException();
        }

        private LibraryReadModel Map(DataLayer.Models.Library dataModelLibrary)
        {
            return new LibraryReadModel
            {
                Id = dataModelLibrary.Id,
                EmailAddress = dataModelLibrary.EmailAddress,
                Name = dataModelLibrary.Name,
                PhoneNumber = dataModelLibrary.PhoneNumber,
                PhysicalAddress = dataModelLibrary.PhysicalAddress
            };
        }

        private DataLayer.Models.Library Map(Guid id, LibraryWriteModel libraryWriteModel)
        {
            return new DataLayer.Models.Library
            {
                Id = id,
                Name = libraryWriteModel.Name,
                EmailAddress = libraryWriteModel.EmailAddress,
                PhoneNumber = libraryWriteModel.PhoneNumber,
                PhysicalAddress = libraryWriteModel.PhysicalAddress
            };
        }
    }
}
