﻿using System;
using System.Collections.Generic;
using BusinessLayer.Models;

namespace BusinessLayer
{
    public interface ILibraryService
    {
        public Guid AddLibrary(LibraryWriteModel library);

        public IEnumerable<LibraryReadModel> GetLibraries(bool ascendingOrder);

        public LibraryReadModel GetLibrary(Guid id);

        public bool UpdateLibrary(Guid id, LibraryWriteModel library);
        void DeleteLibrary(int id);
    }
}
