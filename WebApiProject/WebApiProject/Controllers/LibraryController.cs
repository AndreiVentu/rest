using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ApiLayer.Models;
using BusinessLayer;
using ApiLayer;

namespace LibraryProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibrariesController : ControllerBase
    {
        private ILibraryService libraryService;
        public LibrariesController(ILibraryService libraryService)
        {
            this.libraryService = libraryService;
        }

        [HttpGet()]
        public IActionResult Get([FromQuery]bool ascendingOrder=true)
        {
            var libraries = libraryService.GetLibraries(ascendingOrder);

            return Ok(libraries.Select(x => Mapper.MapToResponseModel(x)));
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var library = libraryService.GetLibrary(id);

            if(library == null)
            {
                return NotFound();
            }

            return Ok(Mapper.MapToResponseModel(library));
        }

        [HttpPost]
        public IActionResult Post([FromBody] LibraryRequestModel library)
        {
            var createdId = libraryService.AddLibrary(Mapper.MapToWriteModel(library));

            return Created($"api/library/{createdId}", createdId);
        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] LibraryRequestModel library)
        {
            var foundElement = libraryService.UpdateLibrary(id, Mapper.MapToWriteModel(library));
            if(foundElement)
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
