﻿using ApiLayer.Models;
using BusinessLayer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiLayer.Controllers
{
    public class BookController: ControllerBase
    {
        private ILibraryService BookService;
        public BookController(ILibraryService BookService)
        {
            this.BookService = BookService;
        }
        [HttpGet()]
        public IActionResult Get([FromQuery] bool ascendingOrder = true)
        {
            var libraries = BookService.GetLibraries(ascendingOrder);

            return Ok(libraries.Select(x => Mapper.MapToResponseModel(x)));
        }
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var library = BookService.GetLibrary(id);

            if (library == null)
            {
                return NotFound();
            }

            return Ok(Mapper.MapToResponseModel(library));
        }
        [HttpPost]
        public IActionResult Post([FromBody] LibraryRequestModel library)
        {
            var createdId = BookService.AddLibrary(Mapper.MapToWriteModel(library));

            return Created($"api/library/{createdId}", createdId);
        }
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] LibraryRequestModel library)
        {
            var foundElement = BookService.UpdateLibrary(id, Mapper.MapToWriteModel(library));
            if (foundElement)
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {

            BookService.DeleteLibrary(id);
            return Ok();
        }
	}
}
