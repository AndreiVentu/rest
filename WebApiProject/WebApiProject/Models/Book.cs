﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiLayer.Models
{
    public class Book
    {
        public Guid Id { get; set; }
        public LibraryResponseModel library { get; set; }
    }
}
