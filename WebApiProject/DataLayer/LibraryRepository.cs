﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Models;

namespace DataLayer
{
    public class LibraryRepository : ILibraryRepository
    {
        public List<Library> Libraries { get; }

        public LibraryRepository()
        {
            Libraries = new List<Library>
            {
                new Library
                {
                    Id = Guid.Parse("97338924-a1b6-4fa2-9f1a-992e2376a291"),
                    Name = "Libris",
                    EmailAddress = "library1@citeste.ro",
                    PhoneNumber = "1385739214",
                    PhysicalAddress = "str. Librariei nr. 1"
                },
                new Library
                {
                    Id = Guid.Parse("5f6fdf50-6e23-432b-9f29-9e19e18d26c1"),
                    Name = "Carturesti",
                    EmailAddress = "carturest@citeste.ro",
                    PhoneNumber = "28475902734",
                    PhysicalAddress = "str. Palas nr. 1"
                }
            };
        }

        public IEnumerable<Library> GetLibraries()
        {
            return Libraries;
        }

        public Library GetLibrary(Guid id)
        {
            return Libraries.FirstOrDefault(x => x.Id == id);
        }

        public void AddLibrary(Library library)
        {
            Libraries.Add(library);
        }

        public bool UpdateLibrary(Library library)
        {
            var index = Libraries.FindIndex(x => x.Id == library.Id);
            if(index == -1)
            {
                return false;
            }

            Libraries[index] = library;

            return true;
        }
    }
}
