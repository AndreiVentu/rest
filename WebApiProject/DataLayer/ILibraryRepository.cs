﻿using System;
using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer
{
    public interface ILibraryRepository
    {
        IEnumerable<Library> GetLibraries();

        void AddLibrary(Library library);

        Library GetLibrary(Guid id);

        bool UpdateLibrary(Library library);
    }
}
